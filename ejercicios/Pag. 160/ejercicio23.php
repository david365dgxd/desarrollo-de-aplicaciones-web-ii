<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 23</title>
	<link rel="stylesheet" type="text/css" href="estilos23.css">
</head>
<body>
	<h1>Nombre del Operador</h1>
	<div class="operador">
		<p>+</p>
		<p><?php echo nombreOperador('+'); ?></p>
	</div>
	<div class="operador">
		<p>-</p>
		<p><?php echo nombreOperador('-'); ?></p>
	</div>
	<div class="operador">
		<p>*</p>
		<p><?php echo nombreOperador('*'); ?></p>
	</div>
	<div class="operador">
		<p>/</p>
		<p><?php echo nombreOperador('/'); ?></p>
	</div>
	<div class="operador">
		<p>%</p>
		<p><?php echo nombreOperador('%'); ?></p>
	</div>
</body>
</html>
