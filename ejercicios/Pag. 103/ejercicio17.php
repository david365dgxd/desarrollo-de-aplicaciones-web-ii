<?php
if(isset($_POST['btnCalcular'])){
    $saldo_anterior = (float)$_POST['txtSaldoAnterior'];
    $tipo_movimiento = $_POST['rdTipoMovimiento'];
    $monto_transaccion = (float)$_POST['txtMontoTransaccion'];

    if ($tipo_movimiento == 'R') {
        $saldo_actual = $saldo_anterior - $monto_transaccion;
    } elseif ($tipo_movimiento == 'D') {
        $saldo_actual = $saldo_anterior + $monto_transaccion;
    } else {
        $saldo_actual = $saldo_anterior;
    }
} else {
    $saldo_anterior = 0;
    $tipo_movimiento = '';
    $monto_transaccion = 0;
    $saldo_actual = 0;
}
?>

<html>
<head>
    <title>Ejercicio 17</title>
</head>
<body>
    <form method="POST">
        <label for="txtSaldoAnterior">Saldo anterior:</label>
        <input type="number" name="txtSaldoAnterior" id="txtSaldoAnterior" value="<?php echo $saldo_anterior ?>" required>
        <br>
        <label>Tipo de movimiento:</label>
        <input type="radio" name="rdTipoMovimiento" id="rdRetiro" value="R" <?php if ($tipo_movimiento == 'R') {echo 'checked';} ?>>
        <label for="rdRetiro">Retiro</label>
        <input type="radio" name="rdTipoMovimiento" id="rdDeposito" value="D" <?php if ($tipo_movimiento == 'D') {echo 'checked';} ?>>
        <label for="rdDeposito">Depósito</label>
        <input type="radio" name="rdTipoMovimiento" id="rdNinguno" value="N" <?php if ($tipo_movimiento == '') {echo 'checked';} ?>>
        <label for="rdNinguno">Ninguno</label>
        <br>
        <label for="txtMontoTransaccion">Monto de la transacción:</label>
        <input type="number" name="txtMontoTransaccion" id="txtMontoTransaccion" value="<?php echo $monto_transaccion ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <div>
        <p>Saldo actual: <?php echo $saldo_actual ?></p>
    </div>
</body>
</html>
