<!DOCTYPE html>
<html>
<head>
	<title>Descuento por género y tarjeta</title>
    <link rel="stylesheet" type="text/css" href="estilos25.css">
</head>
<body>
	<form method="POST">
		<label for="genero">Género:</label>
		<select id="genero" name="genero">
			<option value="Hombre">Hombre</option>
			<option value="Mujer">Mujer</option>
		</select><br>

		<label for="tarjeta">Tipo de tarjeta:</label>
		<select id="tarjeta" name="tarjeta">
			<option value="Obrero">Obrero</option>
			<option value="Empleado">Empleado</option>
		</select><br>

		<label for="sueldo">Sueldo:</label>
		<input type="number" id="sueldo" name="sueldo"><br>

		<input type="submit" value="Calcular descuento">
	</form>

	<?php
		if(isset($_POST['genero']) && isset($_POST['tarjeta']) && isset($_POST['sueldo'])) {
			$genero = $_POST['genero'];
			$tarjeta = $_POST['tarjeta'];
			$sueldo = $_POST['sueldo'];

			if($tarjeta == 'Obrero') {
				if($genero == 'Hombre') {
					$descuento = $sueldo * 0.15;
				} else {
					$descuento = $sueldo * 0.10;
				}
			} else {
				if($genero == 'Hombre') {
					$descuento = $sueldo * 0.20;
				} else {
					$descuento = $sueldo * 0.15;
				}
			}

			echo '<p>El descuento es de: $'. $descuento .'</p>';
		}
	?>
</body>
</html>
