<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Días restantes del año</title>
	<link rel="stylesheet" type="text/css" href="estilos30.css">
</head>
<body>
	<div class="container">
		<h1>Días restantes del año</h1>
		<form method="post" action="ejercicio30.php">
			<label for="fecha">Ingrese una fecha:</label>
			<input type="date" id="fecha" name="fecha" required>
			<button type="submit">Calcular</button>
		</form>
		<?php
			if (isset($_POST['fecha'])) {
				$fecha = new DateTime($_POST['fecha']);
				$hoy = new DateTime();
				$dias_restantes = $hoy->diff($fecha)->format('%a');
				$dias_restantes = $dias_restantes < 0 ? 0 : $dias_restantes;
		?>
				<p>Faltan <?php echo $dias_restantes ?> días para que acabe el año.</p>
		<?php
			}
		?>
	</div>
</body>
</html>
