<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Ejercicio 65</title>
    <link rel="stylesheet" href="estilos65.css">
</head>

<body>
    <form method="post" action="">
        <label for="frase">Frase:</label>
        <input type="text" id="frase" name="frase" required>

        <label for="palabra">Palabra:</label>
        <input type="text" id="palabra" name="palabra" required>

        <input type="submit" value="Buscar">

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $frase = $_POST["frase"];
            $palabra = $_POST["palabra"];

            if (strpos($frase, $palabra) !== false) {
                echo "<p class='resultado'>La palabra '$palabra' se encuentra en la frase.</p>";
            } else {
                echo "<p class='resultado'>La palabra '$palabra' no se encuentra en la frase.</p>";
            }
        }
        ?>
    </form>
</body>

</html>