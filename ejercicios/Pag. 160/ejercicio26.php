<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Frutería</title>
	<link rel="stylesheet" type="text/css" href="estilos26.css">
</head>
<body>
	<div class="container">
		<h1>Frutería</h1>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			<label for="kilos">Cantidad de kilos de manzanas:</label>
			<input type="number" id="kilos" name="kilos" min="0" step="0.01" required>
			<button type="submit">Calcular</button>
		</form>
		<?php
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$kilos = $_POST['kilos'];
				$descuento = 0;

				if ($kilos > 0 && $kilos <= 2) {
					$descuento = 0;
				} elseif ($kilos > 2 && $kilos <= 5) {
					$descuento = 10;
				} elseif ($kilos > 5 && $kilos <= 10) {
					$descuento = 20;
				} elseif ($kilos > 10) {
					$descuento = 30;
				}

				$precio = $kilos * 2; // precio por kilo: $2
				$descuento_aplicado = $precio * $descuento / 100;
				$precio_con_descuento = $precio - $descuento_aplicado;
		?>
				<p>Usted compró <?php echo $kilos ?> kilos de manzanas.</p>
				<p>El precio por kilo es $2.</p>
				<p>El descuento aplicado es <?php echo $descuento ?>%.</p>
				<p>El monto del descuento es $<?php echo number_format($descuento_aplicado, 2) ?>.</p>
				<p>El precio a pagar es $<?php echo number_format($precio_con_descuento, 2) ?>.</p>
		<?php
			}
		?>
	</div>
</body>
</html>
