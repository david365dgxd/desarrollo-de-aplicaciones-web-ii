<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 33 - Suma y producto de múltiplos de 3</title>
	<link rel="stylesheet" type="text/css" href="estilos33.css">
</head>
<body>
	<div class="container">
		<h1>Suma y producto de los N primeros múltiplos de 3</h1>
		<form method="post">
			<label for="n">Ingrese un número N:</label>
			<input type="number" name="n" id="n" required>
			<input type="submit" value="Calcular">
		</form>
		<?php

		if (isset($_POST['n'])) {
			$n = $_POST['n'];

			if ($n <= 0) {
				echo "<p class='error'>Error: N debe ser mayor a 0</p>";
			} else {

				$suma = 0;
				$producto = 1;

				for ($i = 1; $i <= $n; $i++) {
					$multiplo = $i * 3;
					$suma += $multiplo;
					$producto *= $multiplo;
				}

				echo "<p>Suma de los $n primeros múltiplos de 3: $suma</p>";
				echo "<p>Producto de los $n primeros múltiplos de 3: $producto</p>";
			}
		}
		?>
	</div>
</body>
</html>
