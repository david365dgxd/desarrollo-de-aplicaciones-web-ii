<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 32</title>
	<link rel="stylesheet" type="text/css" href="estilos32.css">
</head>
<body>
	<form method="post">
		<label for="inicio">Inicio:</label>
		<input type="number" id="inicio" name="inicio" required>
		<br>
		<label for="fin">Fin:</label>
		<input type="number" id="fin" name="fin" required>
		<br>
		<input type="submit" value="Calcular">
	</form>

	<?php

	if (isset($_POST['inicio']) && isset($_POST['fin'])) {
		$inicio = $_POST['inicio'];
		$fin = $_POST['fin'];


		$pares = 0;
		$impares = 0;


		for ($i = $inicio; $i <= $fin; $i++) {

		    if ($i % 5 == 0) {
		        continue;
		    }

		    if ($i % 2 == 0) {
		        $pares++;
		    }

		    else {
		        $impares++;
		    }
		}

		// Mostramos los resultados
		echo "<div class='resultado'>";
		echo "<p>Cantidad de números pares: " . $pares . "</p>";
		echo "<p>Cantidad de números impares: " . $impares . "</p>";
		echo "</div>";
	}
	?>

</body>
</html>
