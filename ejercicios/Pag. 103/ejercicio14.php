<?php
if(isset($_POST['btnCalcular'])){
    $numero = (int)$_POST['txtNumero'];

    if ($numero > 0) {
        $resultado = "El doble del número es " . ($numero * 2);
    } else if ($numero < 0) {
        $resultado = "El triple del número es " . ($numero * 3);
    } else {
        $resultado = "El número es neutro y su valor es cero";
    }
} else {
    $numero = 0;
    $resultado = "";
}
?>

<html>
<head>
    <title>Ejercicio 14</title>
    <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
    <div class="contenedor">
        <h1>Programa para calcular el doble o triple de un número</h1>
        <form method="POST">
            <label for="txtNumero">Ingrese el número:</label>
            <input type="number" name="txtNumero" id="txtNumero" value="<?php echo $numero ?>" required>
            <br>
            <input type="submit" name="btnCalcular" value="Calcular">
        </form>
        <br>
        <?php
        if(isset($_POST['btnCalcular'])){
            echo "<p>$resultado</p>";
        }
        ?>
    </div>
</body>
</html>
