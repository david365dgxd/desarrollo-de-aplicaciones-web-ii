<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 40 - Máximo común divisor (factorización simultánea)</title>
    <link rel="stylesheet" href="estilos40.css">
</head>
<body>
    <h1>Ejercicio 40 - Máximo común divisor (factorización simultánea)</h1>
    <form action="index.php" method="POST">
        <label for="numero1">Ingrese el primer número:</label>
        <input type="number" name="numero1" id="numero1" required>
        <br>
        <label for="numero2">Ingrese el segundo número:</label>
        <input type="number" name="numero2" id="numero2" required>
        <br>
        <input type="submit" value="Calcular MCD">
    </form>
    <?php
    if (isset($_POST['numero1']) && isset($_POST['numero2'])) {
        $num1 = $_POST['numero1'];
        $num2 = $_POST['numero2'];
        $factores1 = factorizacion($num1);
        $factores2 = factorizacion($num2);
        $mcd = maximo_comun_divisor($factores1, $factores2);
        echo "<p>El máximo común divisor de $num1 y $num2 es: $mcd</p>";
    }

    function factorizacion($numero) {
        $factores = array();
        for ($i = 2; $i <= $numero; $i++) {
            while ($numero % $i == 0) {
                $factores[] = $i;
                $numero /= $i;
            }
        }
        return $factores;
    }

    function maximo_comun_divisor($factores1, $factores2) {
        $mcd = 1;
        for ($i = 0; $i < count($factores1); $i++) {
            if (in_array($factores1[$i], $factores2)) {
                $indice = array_search($factores1[$i], $factores2);
                $mcd *= $factores1[$i];
                unset($factores2[$indice]);
            }
        }
        return $mcd;
    }
    ?>
</body>
</html>
