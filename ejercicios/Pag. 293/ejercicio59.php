<!DOCTYPE html>
<html>

<head>
    <title>Multiplicación de matrices</title>
    <link rel="stylesheet" type="text/css" href="estilos59.css">
</head>

<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label>Ingrese los valores de la matriz A:</label>
        <input type="number" name="a11" placeholder="A[1][1]">
        <input type="number" name="a12" placeholder="A[1][2]">
        <input type="number" name="a21" placeholder="A[2][1]">
        <input type="number" name="a22" placeholder="A[2][2]">

        <label>Ingrese los valores de la matriz B:</label>
        <input type="number" name="b11" placeholder="B[1][1]">
        <input type="number" name="b12" placeholder="B[1][2]">
        <input type="number" name="b21" placeholder="B[2][1]">
        <input type="number" name="b22" placeholder="B[2][2]">

        <input type="submit" name="submit" value="Multiplicar matrices">

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $a11 = $_POST["a11"];
            $a12 = $_POST["a12"];
            $a21 = $_POST["a21"];
            $a22 = $_POST["a22"];

            $b11 = $_POST["b11"];
            $b12 = $_POST["b12"];
            $b21 = $_POST["b21"];
            $b22 = $_POST["b22"];

            $c11 = $a11 * $b11 + $a12 * $b21;
            $c12 = $a11 * $b12 + $a12 * $b22;
            $c21 = $a21 * $b11 + $a22 * $b21;
            $c22 = $a21 * $b12 + $a22 * $b22;

            echo "<div class='resultado'>";
            echo "<p>Matriz resultante C = A * B:</p>";
            echo "<table>";
            echo "<tr><td>$c11</td><td>$c12</td></tr>";
            echo "<tr><td>$c21</td><td>$c22</td></tr>";
            echo "</table>";
            echo "</div>";
        }
        ?>
    </form>
</body>

</html>