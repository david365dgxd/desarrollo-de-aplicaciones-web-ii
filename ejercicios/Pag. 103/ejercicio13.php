<?php
if(isset($_POST['btnComparar'])){
    $num1 = (int)$_POST['txtNum1'];
    $num2 = (int)$_POST['txtNum2'];

    if ($num1 == $num2) {
        $resultado = "Los números ingresados son iguales";
    } else {
        $resultado = "Los números ingresados son diferentes";
    }
} else {
    $num1 = 0;
    $num2 = 0;
    $resultado = "";
}
?>

<html>
<head>
    <title>Ejercicio 13</title>
    <link rel="stylesheet" type="text/css" href="estilos13.css">
</head>
<body>
    <form method="POST">
        <label for="txtNum1">Ingrese el primer número:</label>
        <input type="number" name="txtNum1" id="txtNum1" value="<?php echo $num1 ?>" required>
        <br>
        <label for="txtNum2">Ingrese el segundo número:</label>
        <input type="number" name="txtNum2" id="txtNum2" value="<?php echo $num2 ?>" required>
        <br>
        <input type="submit" name="btnComparar" value="Comparar">
    </form>
    <br>
    <?php
    if(isset($_POST['btnComparar'])){
        echo "<div class='contenedor'>";
        echo "<p>$resultado</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
