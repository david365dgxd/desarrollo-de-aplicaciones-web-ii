<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Ejercicio 67</title>
    <link rel="stylesheet" href="estilos67.css">
</head>

<body>
    <div class="container">
        <h1>Palíndromos</h1>
        <form method="POST">
            <label for="frase">Ingrese una frase:</label>
            <input type="text" id="frase" name="frase" required>
            <input type="submit" value="Buscar">
        </form>
        <?php
        if (isset($_POST['frase'])) {
            $frase = $_POST['frase'];
            $palabras = explode(" ", $frase);
            $palindromos = 0;
            foreach ($palabras as $palabra) {
                $palabra_al_reves = strrev($palabra);
                if (strtolower($palabra) == strtolower($palabra_al_reves)) {
                    $palindromos++;
                }
            }
            echo "<div class='resultado'>";
            if ($palindromos == 1) {
                echo "La frase contiene 1 palabra palíndromo.";
            } else {
                echo "La frase contiene $palindromos palabras palíndromos.";
            }
            echo "</div>";
        }
        ?>
    </div>
</body>

</html>