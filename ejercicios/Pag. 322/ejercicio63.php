<!DOCTYPE html>
<html>

<head>
    <title>Ejercicio 63</title>
    <link rel="stylesheet" type="text/css" href="estilos63.css">
</head>

<body>
    <div class="container">
        <form method="post">
            <label for="frase">Ingrese una frase:</label>
            <input type="text" name="frase" id="frase" placeholder="Ej: Hola mundo">
            <input type="submit" value="Enviar">
        </form>
        <?php
        if (isset($_POST['frase'])) {
            $frase = $_POST['frase'];
            $fraseAsteriscos = str_replace(' ', '*', $frase);
            echo "<div class='resultado'>Frase con asteriscos: $fraseAsteriscos</div>";
        }
        ?>
    </div>
</body>

</html>