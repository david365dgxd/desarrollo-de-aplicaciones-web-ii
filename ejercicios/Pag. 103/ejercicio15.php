<?php
if(isset($_POST['btnOrdenar'])){
    $num1 = (int)$_POST['txtNum1'];
    $num2 = (int)$_POST['txtNum2'];
    $num3 = (int)$_POST['txtNum3'];

    $ascendente = array($num1, $num2, $num3);
    sort($ascendente);

    $descendente = array($num1, $num2, $num3);
    rsort($descendente);

    $resultado_ascendente = implode(", ", $ascendente);
    $resultado_descendente = implode(", ", $descendente);
} else {
    $num1 = 0;
    $num2 = 0;
    $num3 = 0;
    $resultado_ascendente = "";
    $resultado_descendente = "";
}
?>

<html>
<head>
    <title>Ejercicio 15</title>
    <link rel="stylesheet" type="text/css" href="estilos15.css">
</head>
<body>
    <form method="POST">
        <label for="txtNum1">Ingrese el primer número:</label>
        <input type="number" name="txtNum1" id="txtNum1" value="<?php echo $num1 ?>" required>
        <br>
        <label for="txtNum2">Ingrese el segundo número:</label>
        <input type="number" name="txtNum2" id="txtNum2" value="<?php echo $num2 ?>" required>
        <br>
        <label for="txtNum3">Ingrese el tercer número:</label>
        <input type="number" name="txtNum3" id="txtNum3" value="<?php echo $num3 ?>" required>
        <br>
        <input type="submit" name="btnOrdenar" value="Ordenar">
    </form>
    <br>
    <?php
    if(isset($_POST['btnOrdenar'])){
        echo "<div class='contenedor'>";
        echo "<p>Números ordenados en forma ascendente: $resultado_ascendente</p>";
        echo "<p>Números ordenados en forma descendente: $resultado_descendente</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
