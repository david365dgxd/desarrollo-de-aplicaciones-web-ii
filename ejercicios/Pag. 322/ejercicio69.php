<!DOCTYPE html>
<html>

<head>
    <title>Encriptar Frase</title>
    <link rel="stylesheet" type="text/css" href="estilos69.css">
</head>

<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="frase">Ingrese la frase a encriptar:</label>
        <input type="text" name="frase" id="frase" required>
        <input type="submit" value="Encriptar">
        <div class="resultado">
            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $frase = $_POST["frase"];
                $longitud = strlen($frase);
                $frase_encriptada = "";
                for ($i = 0; $i < $longitud; $i++) {
                    $ascii = ord($frase[$i]) + 2;
                    $frase_encriptada .= chr($ascii);
                }
                echo "<p>Frase encriptada: <span>" . $frase_encriptada . "</span></p>";
            }
            ?>
        </div>
    </form>
</body>

</html>