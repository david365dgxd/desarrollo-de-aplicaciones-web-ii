<?php
session_start();

if (isset($_SESSION['user_id'])) {
  header('Location: /php-login');
}

require 'database.php';

$message = '';

if (!empty($_POST['email']) && !empty($_POST['password'])) {
  $records = $conn->prepare('SELECT id, email, password FROM users WHERE email = :email');
  $records->bindParam(':email', $_POST['email']);
  $records->execute();
  $results = $records->fetch(PDO::FETCH_ASSOC);

  if ($results && password_verify($_POST['password'], $results['password'])) {
    $_SESSION['user_id'] = $results['id'];
    header("Location: /php-login");
  } else {
    $message = 'Perdón, los datos ingresados son incorrectos. Verifique sus datos.';
  }
}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="form-container">
      <?php require 'partials/header.php' ?>

      <?php if(!empty($message)): ?>
        <p><?= $message ?></p>
      <?php endif; ?>

      <h1>Ingresa</h1>
      <span>o <a href="signup.php">registrate</a></span>

      <form action="login.php" method="POST">
        <input name="email" type="text" placeholder="Enter your email">
        <input name="password" type="password" placeholder="Enter your Password">
        <input type="submit" value="Submit">
      </form>

      <img src="waza.jpg" alt="Background Image" class="background-image">
    </div>
  </body>
</html>
