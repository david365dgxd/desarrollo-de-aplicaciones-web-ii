<?php
if (isset($_POST['btnVerificar'])) {
    $edad = (int)$_POST['txtEdad'];
    if ($edad >= 18) {
        $mensaje = "Es mayor de edad";
    } else {
        $mensaje = "Es menor de edad";
    }
} else {
    $edad = 0;
    $mensaje = "";
}
?>

<html>

<link rel="stylesheet" type="text/css" href="estilos11.css">

<head>
    <title>Ejercicio 11</title>
</head>

<body>
    <form method="POST">
        <label for="txtEdad">Ingrese su edad:</label>
        <input type="number" name="txtEdad" id="txtEdad" value="<?php echo $edad ?>" required>
        <br>
        <input type="submit" name="btnVerificar" value="Verificar">
    </form>
    <br>
    <div class="contenedor">
        <p><?php echo $mensaje ?></p>
    </div>
</body>

</html>
