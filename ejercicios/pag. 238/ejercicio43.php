<?php
if (isset($_POST['btnCalcular'])) {

    $a = (int)$_POST['txtA'];
    $b = (int)$_POST['txtB'];

    $pares = 0;
    $impares = 0;
    $multiplos3 = 0;

    $suma_pares = 0;
    $suma_impares = 0;
    $suma_multiplos3 = 0;

    for ($i = $a; $i <= $b; $i++) {
        if ($i % 2 == 0) {
            $pares++;
            $suma_pares += $i;
        } else {
            $impares++;
            $suma_impares += $i;
        }
        if ($i % 3 == 0) {
            $multiplos3++;
            $suma_multiplos3 += $i;
        }
    }

    echo "<h2>Resultados:</h2>";
    echo "<p>Números pares: $pares (suma: $suma_pares)</p>";
    echo "<p>Números impares: $impares (suma: $suma_impares)</p>";
    echo "<p>Múltiplos de 3: $multiplos3 (suma: $suma_multiplos3)</p>";
} else {

    $a = 0;
    $b = 0;
}
?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="estilos43.css">
<head>
    <title>ejercicio 43</title>
</head>

<body>
    <form method="POST">
        <label for="txtA">Ingrese el primer número:</label>
        <input type="number" name="txtA" id="txtA" value="<?php echo $a ?>" required>
        <br>
        <label for="txtB">Ingrese el segundo número:</label>
        <input type="number" name="txtB" id="txtB" value="<?php echo $b ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
</body>

</html>