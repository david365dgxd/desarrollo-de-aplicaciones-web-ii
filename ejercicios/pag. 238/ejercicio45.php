<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];

    $contador = 0;
    for ($i = $inicio; $i <= $fin; $i++) {
        $reverso = strrev($i);
        if ($i == $reverso) {
            $contador++;
        }
    }
} else {
    $contador = "";
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Números capicúa</title>
    <link rel="stylesheet" type="text/css" href="estilos45.css">
</head>

<body>
    <div class="container">
        <h1>Contador de números capicúa</h1>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <label for="inicio">Inicio del rango:</label>
            <input type="number" name="inicio" id="inicio" required>
            <br><br>
            <label for="fin">Fin del rango:</label>
            <input type="number" name="fin" id="fin" required>
            <br><br>
            <input type="submit" value="Calcular">
        </form>
        <br>
        <?php
        if ($contador != "") {
            echo "<p>Hay " . $contador . " número(s) capicúa en el rango [" . $inicio . ", " . $fin . "].</p>";
        }
        ?>
    </div>
</body>

</html>