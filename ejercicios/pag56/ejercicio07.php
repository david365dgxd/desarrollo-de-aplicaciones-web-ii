<html>
<head>
    <title>Ejercicio 07</title>
    <link rel="stylesheet" type="text/css" href="estilos07.css">
</head>
<body>
    <?php
    if (isset($_POST['btnCalcular'])) {
        //Obtener los valores ingresados por el usuario
        $horas = (int)$_POST['txtHoras'];

        //Realizar las operaciones
        $minutos = $horas * 60;
        $segundos = $horas * 3600;

        //Mostrar los resultados
        echo "<div class='contenedor'>";
        echo "<p>$horas horas son equivalentes a:</p>";
        echo "<ul>";
        echo "<li>$minutos minutos</li>";
        echo "<li>$segundos segundos</li>";
        echo "</ul>";
        echo "</div>";
    }
    ?>
    <form method="POST">
        <label for="txtHoras">Ingrese la cantidad de horas:</label>
        <input type="number" name="txtHoras" id="txtHoras" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
</body>
</html>
