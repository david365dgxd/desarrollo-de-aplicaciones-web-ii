<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 39</title>
	<link rel="stylesheet" type="text/css" href="estilos39.css">
</head>
<body>
	<div class="container">
		<h1>Ejercicio 39 - Máximo Común Divisor</h1>
		<form method="post">
			<label for="numero1">Ingrese el primer número:</label>
			<input type="number" id="numero1" name="numero1" required>
			<label for="numero2">Ingrese el segundo número:</label>
			<input type="number" id="numero2" name="numero2" required>
			<button type="submit">Calcular MCD</button>
		</form>
		<?php
		// Función para calcular el MCD usando el método de Euclides
		function euclides($a, $b) {
			while ($b != 0) {
				$r = $a % $b;
				$a = $b;
				$b = $r;
			}
			return $a;
		}

		// Verificamos si se han enviado los datos del formulario
		if (isset($_POST['numero1']) && isset($_POST['numero2'])) {
			// Obtenemos los números ingresados por el usuario
			$num1 = $_POST['numero1'];
			$num2 = $_POST['numero2'];

			// Calculamos el MCD usando el método de Euclides
			$mcd = euclides($num1, $num2);

			 // Mostramos el resultado
			echo "<p>El MCD de $num1 y $num2 es: $mcd</p>";
		}
		?>
	</div>
</body>
</html>
