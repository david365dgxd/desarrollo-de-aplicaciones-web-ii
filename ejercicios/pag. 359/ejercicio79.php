<?php
function esPalindromo($palabra)
{
    // Convertir la palabra a minúsculas y eliminar espacios en blanco
    $palabra = strtolower(str_replace(' ', '', $palabra));
    // Obtener la palabra al revés
    $palabraInvertida = strrev($palabra);
    // Comparar la palabra original con la invertida
    return ($palabra === $palabraInvertida);
}

if (isset($_POST['palabra'])) {
    $palabra = $_POST['palabra'];
    $esPalindromo = esPalindromo($palabra);
} else {
    $esPalindromo = null;
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Ejercicio 79</title>
    <link rel="stylesheet" type="text/css" href="estilos79.css">
</head>

<body>
    <div class="container">
        <h1>Palíndromo</h1>
        <form method="POST">
            <label for="palabra">Ingrese una palabra:</label>
            <input type="text" id="palabra" name="palabra" required>
            <button type="submit">Verificar</button>
        </form>
        <?php if (isset($esPalindromo)) : ?>
            <p>La palabra "<?php echo $palabra ?>"
                <?php echo ($esPalindromo ? "es" : "no es") ?> palíndromo</p>
        <?php endif ?>
    </div>
</body>

</html>