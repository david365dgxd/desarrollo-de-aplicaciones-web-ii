<?php
if(isset($_POST['btnCalcular'])){
    // Obtener el valor del lado del cuadrado
    $lado = (float)$_POST['txtLado'];

    // Calcular el área del cuadrado
    $area = $lado * $lado;

    // Calcular el perímetro del cuadrado
    $perimetro = 4 * $lado;
}
else{
    // Si el usuario no ha enviado el formulario, se asignan valores iniciales
    $lado = 0;
    $area = 0;
    $perimetro = 0;
}
?>

<html>
<head>
    <title>Ejercicio 06</title>
    <link rel="stylesheet" type="text/css" href="estilos06.css">
</head>
<body>
    <form method="POST">
        <label for="txtLado">Ingrese el valor del lado del cuadrado:</label>
        <input type="number" step="0.01" name="txtLado" id="txtLado" value="<?php echo $lado ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <?php
    // Mostrar los resultados si se han realizado las operaciones
    if(isset($_POST['btnCalcular'])){
        echo "<div class='contenedor'>";
        echo "<p>El área del cuadrado es: $area</p>";
        echo "<p>El perímetro del cuadrado es: $perimetro</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
