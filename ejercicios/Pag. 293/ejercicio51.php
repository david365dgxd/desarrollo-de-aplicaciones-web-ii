<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="estilos51.css">

<head>
    <title>Suma y promedio de números</title>
</head>

<body>
    <form method="post" action="ejercicio51.php">
        <label for="num1">Número 1:</label>
        <input type="number" name="num1" id="num1"><br>

        <label for="num2">Número 2:</label>
        <input type="number" name="num2" id="num2"><br>

        <label for="num3">Número 3:</label>
        <input type="number" name="num3" id="num3"><br>

        <label for="num4">Número 4:</label>
        <input type="number" name="num4" id="num4"><br>

        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["num1"]) && isset($_POST["num2"]) && isset($_POST["num3"]) && isset($_POST["num4"])) {
        $vector = array($_POST["num1"], $_POST["num2"], $_POST["num3"], $_POST["num4"]);
        $suma = array_sum($vector);
        $promedio = $suma / count($vector);
        echo "La suma es: $suma y el promedio es: $promedio";
    }
    ?>
</body>

</html>