<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 37</title>
	<link rel="stylesheet" type="text/css" href="estilos37.css">
</head>
<body>
	<h1>Determinar cantidad de números primos en un rango</h1>
	<form method="POST">
		<label for="inicio">Inicio del rango:</label>
		<input type="number" id="inicio" name="inicio" required>
		<br>
		<label for="fin">Fin del rango:</label>
		<input type="number" id="fin" name="fin" required>
		<br>
		<button type="submit">Calcular</button>
	</form>

	<?php

	function esPrimo($numero) {
	    if ($numero <= 1) {
	        return false;
	    }
	    for ($i = 2; $i <= sqrt($numero); $i++) {
	        if ($numero % $i == 0) {
	            return false;
	        }
	    }
	    return true;
	}

	if (isset($_POST['inicio']) && isset($_POST['fin'])) {
	    $inicio = $_POST['inicio'];
	    $fin = $_POST['fin'];

	    if ($inicio > $fin) {
	        echo "<p class='error'>El inicio del rango debe ser menor o igual que el fin del rango.</p>";
	    } else {

	        $cantidadPrimos = 0;

	        for ($i = $inicio; $i <= $fin; $i++) {
	            if (esPrimo($i)) {
	                $cantidadPrimos++;
	            }
	        }

	        echo "<p class='resultado'>En el rango del $inicio al $fin hay $cantidadPrimos número(s) primo(s).</p>";
	    }
	}
	?>

</body>
</html>
