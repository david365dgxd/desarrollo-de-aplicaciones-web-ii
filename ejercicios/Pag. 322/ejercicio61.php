<!DOCTYPE html>
<html>

<head>
    <title>Bienvenido a su tienda de preferencia</title>
    <link rel="stylesheet" type="text/css" href="estilos61.css">
</head>

<body>
    <form method="post" action="">
        <label for="nombre">Ingrese su nombre:</label>
        <input type="text" id="nombre" name="nombre" required>
        <input type="submit" value="Enviar">
        <?php
        if (isset($_POST['nombre'])) {
            $nombre = $_POST['nombre'];
            echo "<p class='resultado'>Bienvenido, Sr(a) $nombre, a su tienda de preferencia</p>";
        }
        ?>
    </form>
</body>

</html>