<?php

function descuento($tipo)
{
    if ($tipo == "G") {
        return 0.15;
    } elseif ($tipo == "A") {
        return 0.2;
    } else {
        return 0;
    }
}

function recargo($tipo)
{
    if ($tipo == "G") {
        return 0.1;
    } elseif ($tipo == "A") {
        return 0.05;
    } else {
        return 0;
    }
}

if (isset($_POST['monto'])) {
    $monto = $_POST['monto'];
    $tipo = $_POST['tipo'];
    $forma_pago = $_POST['forma_pago'];

    if ($forma_pago == "C") {
        $desc = $monto * descuento($tipo);
        $total = $monto - $desc;
        $rec = 0;
    } elseif ($forma_pago == "P") {
        $rec = $monto * recargo($tipo);
        $total = $monto + $rec;
        $desc = 0;
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Ejercicio 77</title>
    <link rel="stylesheet" type="text/css" href="estilos77.css">
</head>

<body>

    <div class="container">
        <h1>Calcular descuento o recargo</h1>
        <form method="post">
            <label for="monto">Monto de la compra:</label>
            <input type="number" id="monto" name="monto" required>

            <label for="tipo">Tipo de cliente:</label>
            <select id="tipo" name="tipo" required>
                <option value="G">Público en general</option>
                <option value="A">Cliente afiliado</option>
            </select>

            <label for="forma_pago">Forma de pago:</label>
            <select id="forma_pago" name="forma_pago" required>
                <option value="C">Al contado</option>
                <option value="P">En plazos</option>
            </select>

            <input type="submit" value="Calcular">
        </form>

        <?php if (isset($_POST['monto'])) { ?>
            <div class="resultado">
                <?php if ($forma_pago == "C") { ?>
                    <p>Descuento: <?php echo $desc; ?></p>
                <?php } elseif ($forma_pago == "P") { ?>
                    <p>Recargo: <?php echo $rec; ?></p>
                <?php } ?>
                <p>Total a pagar: <?php echo $total; ?></p>
            </div>
        <?php } ?>

    </div>

</body>

</html>