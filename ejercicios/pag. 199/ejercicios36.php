<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 36</title>
	<link rel="stylesheet" type="text/css" href="estilos36.css">
</head>
<body>
	<h1>Porcentaje de números pares, impares y neutros</h1>
	<form method="post" action="">
		<label for="numero">Ingrese un número:</label>
		<input type="number" name="numero" id="numero" required>
		<input type="submit" value="Calcular">
	</form>
	<?php
	if (isset($_POST['numero'])) {
		$numero = $_POST['numero'];
		$pares = 0;
		$impares = 0;
		$neutros = 0;
		for ($i = 1; $i <= $numero; $i++) {
			if ($i % 2 == 0) {
				$pares++;
			} elseif ($i % 2 == 1) {
				$impares++;
			} else {
				$neutros++;
			}
		}
		$porcentajePares = round(($pares / $numero) * 100, 2);
		$porcentajeImpares = round(($impares / $numero) * 100, 2);
		$porcentajeNeutros = round(($neutros / $numero) * 100, 2);
		echo "<p>Porcentaje de números pares: $porcentajePares%</p>";
		echo "<p>Porcentaje de números impares: $porcentajeImpares%</p>";
		echo "<p>Porcentaje de números neutros: $porcentajeNeutros%</p>";
	}
	?>
</body>
</html>
