<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="estilos41.css">

<head>
    <title>Suma de cuadrados y cubos</title>
</head>

<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $N = intval($_POST["n"]);

        $sum_cuadrados = 0;
        $sum_cubos = 0;

        for ($i = 1; $i <= $N; $i++) {
            $cuadrado_actual = $i * $i;
            $cubo_actual = $i * $i * $i;

            $sum_cuadrados += $cuadrado_actual;
            $sum_cubos += $cubo_actual;
        }


        echo "<h1>Resultados de la suma de cuadrados y cubos</h1>\n";
        echo "<p>La suma de los cuadrados es: " . $sum_cuadrados . "</p>\n";
        echo "<p>La suma de los cubos es: " . $sum_cubos . "</p>\n";
    } else {

        echo "<h1>Ingresa un valor para N</h1>\n";
        echo "<form method=\"post\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\">\n";
        echo "  <input type=\"number\" name=\"n\" required>\n";
        echo "  <input type=\"submit\" value=\"Calcular\">\n";
        echo "</form>\n";
    }
    ?>
</body>

</html>