<?php
if(isset($_POST['btnConvertir'])){
    $grados = (float)$_POST['txtGrados'];
    $centesimales = $grados * 1.1111111;
} else {
    $grados = 0;
    $centesimales = 0;
}
?>

<html>
<link rel="stylesheet" type="text/css" href="estilos10.css">
<head>
    <title>Ejercicio 10</title>
</head>
<body>
    <form method="POST">
        <label for="txtGrados">Ingrese los grados:</label>
        <input type="number" name="txtGrados" id="txtGrados" value="<?php echo $grados ?>" required>
        <br>
        <input type="submit" name="btnConvertir" value="Convertir">
    </form>
    <br>
    <?php
    if(isset($_POST['btnConvertir'])){
        echo "<div class='contenedor'>";
        echo "<p>Los grados sexagesimales $grados son equivalentes a $centesimales centesimales.</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
