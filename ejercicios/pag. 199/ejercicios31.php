<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Factorial</title>
	<link rel="stylesheet" type="text/css" href="estilos31.css">
</head>
<body>
	<div class="container">
		<h1>Factorial</h1>
		<form method="post" action="">
			<label for="numero">Ingrese un número:</label>
			<input type="number" id="numero" name="numero" min="0" required>
			<button type="submit">Calcular factorial</button>
		</form>
		<?php
			if (isset($_POST['numero'])) {
				$numero = $_POST['numero'];
				$factorial = 1;

				for ($i = 1; $i <= $numero; $i++) {
					$factorial *= $i;
				}

				echo "<p>El factorial de $numero es: $factorial</p>";
			}
		?>
	</div>
</body>
</html>
