<?php
if(isset($_POST['btnCalcular'])){
    $lado1 = (int)$_POST['txtLado1'];
    $lado2 = (int)$_POST['txtLado2'];
    $lado3 = (int)$_POST['txtLado3'];

    if($lado1 + $lado2 > $lado3 && $lado1 + $lado3 > $lado2 && $lado2 + $lado3 > $lado1){
        if($lado1 == $lado2 && $lado2 == $lado3){
            $tipoTriangulo = "Triángulo equilátero";
        }elseif($lado1 == $lado2 || $lado1 == $lado3 || $lado2 == $lado3){
            $tipoTriangulo = "Triángulo isósceles";
        }else{
            $tipoTriangulo = "Triángulo escaleno";
        }
    }else{
        $tipoTriangulo = "No es un triángulo";
    }
} else {
    $lado1 = 0;
    $lado2 = 0;
    $lado3 = 0;
    $tipoTriangulo = "";
}
?>

<html>
<head>
    <title>Ejercicio 20</title>
    <link rel="stylesheet" type="text/css" href="estilos20.css">
</head>
<body>
    <form method="POST">
        <label for="txtLado1">Ingrese la longitud del lado 1:</label>
        <input type="number" name="txtLado1" id="txtLado1" value="<?php echo $lado1 ?>" required>
        <br>
        <label for="txtLado2">Ingrese la longitud del lado 2:</label>
        <input type="number" name="txtLado2" id="txtLado2" value="<?php echo $lado2 ?>" required>
        <br>
        <label for="txtLado3">Ingrese la longitud del lado 3:</label>
        <input type="number" name="txtLado3" id="txtLado3" value="<?php echo $lado3 ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <?php
    if(isset($_POST['btnCalcular'])){
        echo "<div class='contenedor'>";
        echo "<p>$tipoTriangulo</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
