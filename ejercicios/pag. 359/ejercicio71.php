<!DOCTYPE html>
<html>

<head>
    <title>Área y Perímetro de un Cuadrado</title>
    <link rel="stylesheet" type="text/css" href="estilos71.css">
</head>

<body>
    <div class="container">
        <h1>Área y Perímetro de un Cuadrado</h1>
        <form method="POST">
            <label for="lado">Ingrese el valor del lado:</label>
            <input type="number" id="lado" name="lado" required>
            <input type="submit" value="Calcular">
        </form>

        <?php
        if (isset($_POST['lado'])) {
            $lado = $_POST['lado'];
            $area = $lado * $lado;
            $perimetro = 4 * $lado;

            echo "<div class='resultado'>";
            echo "<p>El área del cuadrado es: " . $area . "</p>";
            echo "<p>El perímetro del cuadrado es: " . $perimetro . "</p>";
            echo "</div>";
        }
        ?>
    </div>
</body>

</html>