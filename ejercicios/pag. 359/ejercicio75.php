<?php
function clasificarCaracter($caracter)
{
    if (ctype_alpha($caracter)) {
        if (ctype_upper($caracter)) {
            return "Letra mayúscula";
        } elseif (ctype_lower($caracter)) {
            return "Letra minúscula";
        }
    } elseif (ctype_digit($caracter)) {
        return "Número";
    } else {
        return "Símbolo";
    }

    return "Símbolo";
}

if (isset($_POST['caracter'])) {
    $caracter = $_POST['caracter'];
    $clasificacion = clasificarCaracter($caracter);
} else {
    $caracter = "";
    $clasificacion = "";
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Clasificar caracteres</title>
    <link rel="stylesheet" type="text/css" href="estilos75.css">
</head>

<body>
    <div class="container">
        <h1>Clasificación de caracteres</h1>
        <form method="post">
            <label for="caracter">Ingrese un carácter:</label>
            <input type="text" name="caracter" id="caracter" maxlength="1" required>
            <button type="submit">Clasificar</button>
        </form>
        <div class="resultado">
            <?php if ($clasificacion !== "") : ?>
                <p>El carácter ingresado es: <?php echo $clasificacion; ?></p>
            <?php endif; ?>
        </div>
    </div>
</body>

</html>