<!DOCTYPE html>
<html>

<head>
    <title>Cantidad de números primos de n cifras</title>
    <link rel="stylesheet" type="text/css" href="estilos49.css">
</head>

<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $n = intval($_POST["n"]);

        function es_primo($numero)
        {
            if ($numero < 2) {
                return false;
            }
            for ($i = 2; $i <= sqrt($numero); $i++) {
                if ($numero % $i == 0) {
                    return false;
                }
            }
            return true;
        }

        $cantidad_primos = 0;
        $minimo = pow(10, $n - 1);
        $maximo = pow(10, $n) - 1;
        for ($i = $minimo; $i <= $maximo; $i++) {
            if (es_primo($i)) {
                $cantidad_primos++;
            }
        }

        echo "<h1>Cantidad de números primos de " . $n . " cifras</h1>\n";
        echo "<p>Hay " . $cantidad_primos . " números primos de " . $n . " cifras.</p>\n";
    } else {

        echo "<h1>Ingrese el valor de n</h1>\n";
        echo "<form method=\"post\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\">\n";
        echo "  <input type=\"number\" name=\"n\" required>\n";
        echo "  <input type=\"submit\" value=\"Calcular\">\n";
        echo "</form>\n";
    }
    ?>
</body>

</html>