<?php
  session_start();

  require 'database.php';

  if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id, email, password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $user = null;

    if (count($results) > 0) {
      $user = $results;
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bienvenido a mi pagina web</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  
  <body>
    <?php require 'partials/header.php' ?>

    <?php if(!empty($user)): ?>
      <br> Welcome. <?= $user['email']; ?>
      <br>ingresaste exitosamente
      <a href="logout.php">
        registro
      </a>
    <?php else: ?>
      <div class="form-container">
        <img src="waza.jpg" alt="Background Image">
      
        <h1>ingresa o registrate porfavor</h1>

        <a href="login.php" class="button jump">Ingresar</a>
        <a href="signup.php" class="button jump">Registrarse</a>
      </div>

    <?php endif; ?>
  </body>
</html>
