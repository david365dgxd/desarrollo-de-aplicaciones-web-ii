<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 34</title>
	<link rel="stylesheet" type="text/css" href="estilos34.css">
</head>
<body>
	<h1>Determinar cantidad de dígitos 0 en un número</h1>
	<form action="ejercicio34.php" method="POST">
		<label for="numero">Ingrese un número:</label>
		<input type="number" name="numero" id="numero" required>
		<button type="submit">Calcular</button>
	</form>
</body>
</html>
<?php
// Obtenemos el número ingresado por el usuario
$numero = $_POST['numero'];

// Convertimos el número a una cadena de caracteres
$numeroStr = (string)$numero;

// Inicializamos el contador de ceros
$cantCeros = 0;

// Recorremos cada dígito del número
for ($i = 0; $i < strlen($numeroStr); $i++) {
    // Si el dígito es 0, incrementamos el contador de ceros
    if ($numeroStr[$i] == '0') {
        $cantCeros++;
    }
}

// Mostramos el resultado
echo "<h2>El número de ceros en $numero es: $cantCeros</h2>";
?>
