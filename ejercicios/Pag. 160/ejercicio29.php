<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Visitas</title>
	<link rel="stylesheet" type="text/css" href="estilos29.css">
</head>
<body>
	<div class="container">
		<h1>Visitas</h1>
		<form method="post" action="visitas.php">
			<label for="sexo">Sexo:</label>
			<select id="sexo" name="sexo" required>
				<option value="masculino">Masculino</option>
				<option value="femenino">Femenino</option>
			</select>
			<br>
			<label for="puntaje">Puntaje obtenido:</label>
			<input type="number" id="puntaje" name="puntaje" min="0" required>
			<br>
			<button type="submit">Consultar</button>
		</form>

		<?php
			if (isset($_POST['sexo']) && isset($_POST['puntaje'])) {
				$sexo = $_POST['sexo'];
				$puntaje = $_POST['puntaje'];
				$ciudad = "";

				if ($sexo == "masculino") {
					if ($puntaje >= 18 && $puntaje <= 35) {
						$ciudad = "Arequipa";
					} elseif ($puntaje >= 36 && $puntaje <= 75) {
						$ciudad = "Cuzco";
					} elseif ($puntaje > 75) {
						$ciudad = "Iquitos";
					}
				} elseif ($sexo == "femenino") {
					if ($puntaje >= 18 && $puntaje <= 35) {
						$ciudad = "Cuzco";
					} elseif ($puntaje >= 36 && $puntaje <= 75) {
						$ciudad = "Iquitos";
					} elseif ($puntaje > 75) {
						$ciudad = "Arequipa";
					}
				}

				if (!empty($ciudad)) {
					echo "<p>La ciudad que visitará es: $ciudad</p>";
				} else {
					echo "<p>No se encontró una ciudad para los datos ingresados.</p>";
				}
			}
		?>
	</div>
</body>
</html>
