<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Cálculo de utilidades</title>
	<link rel="stylesheet" type="text/css" href="estilos28.css">
</head>
<body>
	<div class="container">
		<h1>Cálculo de utilidades</h1>
		<form method="post" action="utilidades.php">
			<label for="tiempo_servicio">Tiempo de servicio (años):</label>
			<input type="number" id="tiempo_servicio" name="tiempo_servicio" min="0" required>
			<label for="cargo">Cargo:</label>
			<select id="cargo" name="cargo">
				<option value="administrador">Administrador</option>
				<option value="contador">Contador</option>
				<option value="empleado">Empleado</option>
			</select>
			<button type="submit">Calcular</button>
		</form>
		<?php
			if (isset($_POST['tiempo_servicio']) && isset($_POST['cargo'])) {
				$tiempo_servicio = $_POST['tiempo_servicio'];
				$cargo = $_POST['cargo'];
				$utilidades = 0;

				if ($tiempo_servicio >= 0 && $tiempo_servicio <= 2) {
					if ($cargo == 'administrador') {
						$utilidades = 2000;
					} elseif ($cargo == 'contador') {
						$utilidades = 1500;
					} elseif ($cargo == 'empleado') {
						$utilidades = 1000;
					}
				} elseif ($tiempo_servicio >= 3 && $tiempo_servicio <= 5) {
					if ($cargo == 'administrador') {
						$utilidades = 2500;
					} elseif ($cargo == 'contador') {
						$utilidades = 2000;
					} elseif ($cargo == 'empleado') {
						$utilidades = 1500;
					}
				} elseif ($tiempo_servicio >= 6 && $tiempo_servicio <= 8) {
					if ($cargo == 'administrador') {
						$utilidades = 3000;
					} elseif ($cargo == 'contador') {
						$utilidades = 2500;
					} elseif ($cargo == 'empleado') {
						$utilidades = 2000;
					}
				} elseif ($tiempo_servicio > 8) {
					if ($cargo == 'administrador') {
						$utilidades = 4000;
					} elseif ($cargo == 'contador') {
						$utilidades = 3500;
					} elseif ($cargo == 'empleado') {
						$utilidades = 1500;
					}
				}

				echo '<p>El monto que recibirá el trabajador por utilidades es: $' . $utilidades . '</p>';
			}
		?>
	</div>
</body>
</html>
