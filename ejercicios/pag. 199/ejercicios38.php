<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 38 - Números capicúa</title>
	<link rel="stylesheet" type="text/css" href="estilos38.css">
</head>
<body>
	<div class="container">
		<h1>Números capicúa</h1>
		<form method="POST">
			<label for="inicio">Ingrese el número de inicio:</label>
			<input type="number" id="inicio" name="inicio" required>
			<label for="fin">Ingrese el número de fin:</label>
			<input type="number" id="fin" name="fin" required>
			<input type="submit" value="Calcular">
		</form>
		<?php

		function es_capicua($numero) {
			$numero_invertido = strrev($numero);
			return ($numero == $numero_invertido);
		}


		$inicio = $_POST['inicio'];
		$fin = $_POST['fin'];

		$capicuas = 0;

		// Iteramos sobre el rango de números
		for ($i = $inicio; $i <= $fin; $i++) {
			if (es_capicua($i)) {
				$capicuas++;
			}
		}

		echo "<p>La cantidad de números capicúa es: " . $capicuas . "</p>";
		?>
	</div>
</body>
</html>
