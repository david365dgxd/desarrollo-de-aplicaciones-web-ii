<?php
if (isset($_POST['btnCalcular'])) {
    //Obtener los valores ingresados por el usuario
    $a = (int)$_POST['txtA'];
    $b = (int)$_POST['txtB'];

    //Realizar las operaciones
    $suma = $a + $b;
    $resta = $a - $b;
} else {
    //Si el usuario no ha enviado el formulario, se asignan valores iniciales
    $a = 0;
    $b = 0;
    $suma = 0;
    $resta = 0;
}
?>

<html>

<link rel="stylesheet" type="text/css" href="estilos01.css">

<head>
    <title>Ejercicio 01</title>
</head>

<body>
    <form method="POST">
        <label for="txtA">Ingrese el primer numero:</label>
        <input type="number" name="txtA" id="txtA" value="<?php echo $a ?>" required>
        <br>
        <label for="txtB">Ingrese el segundo numero:</label>
        <input type="number" name="txtB" id="txtB" value="<?php echo $b ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <?php
    //Mostrar los resultados si se han realizado las operaciones
    if (isset($_POST['btnCalcular'])) {
        echo "La suma de $a y $b es: $suma <br>";
        echo "La resta de $a y $b es: $resta";
    }
    ?>
</body>

</html>