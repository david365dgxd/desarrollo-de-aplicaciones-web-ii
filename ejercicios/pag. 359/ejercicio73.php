<!DOCTYPE html>
<html>

<head>
    <title>Etapa de vida</title>
    <link rel="stylesheet" type="text/css" href="estilos73.css">
</head>

<body>
    <div class="container">
        <h1>Etapa de vida</h1>
        <form method="post">
            <label for="edad">Ingrese su edad:</label>
            <input type="number" name="edad" id="edad" required>
            <button type="submit">Enviar</button>
        </form>
        <?php
        if (isset($_POST["edad"])) {
            $edad = $_POST["edad"];
            $etapa = Etapa($edad);
            echo "<p>Su etapa de vida es: $etapa</p>";
        }

        function Etapa($edad)
        {
            if ($edad >= 0 && $edad <= 2) {
                return "Bebé";
            } elseif ($edad >= 3 && $edad <= 5) {
                return "Niño";
            } elseif ($edad >= 6 && $edad <= 12) {
                return "Pubertad";
            } elseif ($edad >= 13 && $edad <= 18) {
                return "Adolescente";
            } elseif ($edad >= 19 && $edad <= 25) {
                return "Joven";
            } elseif ($edad >= 26 && $edad <= 60) {
                return "Adulto";
            } else {
                return "Anciano";
            }
        }
        ?>
    </div>
</body>

</html>