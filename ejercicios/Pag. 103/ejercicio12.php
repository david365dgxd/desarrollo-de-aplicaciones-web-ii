<!DOCTYPE html>
<html>
<head>
  <title>Ejercicio 12</title>
  <link rel="stylesheet" type="text/css" href="estilos12.css">
</head>
<body>
  <?php

  // Pedimos al usuario que ingrese dos números enteros
  echo "Ingresa el primer número entero: ";
  $num1 = readline();
  echo "Ingresa el segundo número entero: ";
  $num2 = readline();

  // Comparamos los números ingresados y devolvemos el menor
  if ($num1 < $num2) {
    echo "<p class='mensaje'>El número menor es: " . $num1 . "</p>";
  } else {
    echo "<p class='mensaje'>El número menor es: " . $num2 . "</p>";
  }

  ?>

</body>
</html>
