<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ejercicio 24 - Nombre del canal de televisión</title>
    <link rel="stylesheet" type="text/css" href="estilos24.css">
</head>
<body>
    <h1>Nombre del canal de televisión</h1>
    <?php
        function nombreCanal($numero) {
            switch ($numero) {
                case 2:
                    return 'Canal 2 - América TV';
                case 4:
                    return 'Canal 4 - ATV';
                case 5:
                    return 'Canal 5 - Panamericana TV';
                case 9:
                    return 'Canal 9 - ATV+';
                case 13:
                    return 'Canal 13 - Willax TV';
                default:
                    return 'No se encontró el canal';
            }
        }

        $canal = isset($_GET['canal']) ? $_GET['canal'] : '';

        if ($canal != '') {
            echo '<p>El número del canal es: ' . $canal . '</p>';
            echo '<p>El nombre del canal es: ' . nombreCanal($canal) . '</p>';
        }
    ?>
    <form>
        <label for="canal">Ingrese el número del canal:</label>
        <input type="number" name="canal" id="canal" min="1" max="99" required>
        <br><br>
        <input type="submit" value="Obtener nombre del canal">
    </form>
</body>
</html>
