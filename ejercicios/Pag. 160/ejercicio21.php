<?php
if(isset($_POST['btnEnviar'])){
    $mes = (int)$_POST['txtMes'];

    switch ($mes) {
        case 1:
            $mesLetras = "Enero";
            break;
        case 2:
            $mesLetras = "Febrero";
            break;
        case 3:
            $mesLetras = "Marzo";
            break;
        case 4:
            $mesLetras = "Abril";
            break;
        case 5:
            $mesLetras = "Mayo";
            break;
        case 6:
            $mesLetras = "Junio";
            break;
        case 7:
            $mesLetras = "Julio";
            break;
        case 8:
            $mesLetras = "Agosto";
            break;
        case 9:
            $mesLetras = "Septiembre";
            break;
        case 10:
            $mesLetras = "Octubre";
            break;
        case 11:
            $mesLetras = "Noviembre";
            break;
        case 12:
            $mesLetras = "Diciembre";
            break;
        default:
            $mesLetras = "Número de mes inválido";
            break;
    }
} else {
    $mes = 0;
    $mesLetras = "";
}
?>

<html>
<head>
    <title>Ejercicio 21</title>
    <link rel="stylesheet" type="text/css" href="estilos21.css">
</head>
<body>
    <form method="POST">
        <label for="txtMes">Ingrese el número de mess:</label>
        <input type="number" name="txtMes" id="txtMes" value="<?php echo $mes ?>" required>
        <br>
        <input type="submit" name="btnEnviar" value="Enviar">
    </form>
    <br>
    <?php
    if(isset($_POST['btnEnviar'])){
        echo "<div class='contenedor'>";
        echo "<p>El mes ingresado es: $mesLetras</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
