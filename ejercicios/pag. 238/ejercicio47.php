<!DOCTYPE html>
<html>

<head>
    <title>Suma de serie</title>
    <link rel="stylesheet" type="text/css" href="estilos47.css">
</head>

<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $x = $_POST["x"];
        $n = $_POST["n"];
        $s = 0;
        $factorial = 1;
        for ($i = 0; $i <= $n; $i++) {
            if ($i == 0) {
                $s += 1 / $factorial;
            } else {
                $factorial *= $i;
                $s += pow($x, $i) / $factorial;
            }
        }
        echo "<h1>La suma de la serie es:</h1>\n";
        echo "<p>" . $s . "</p>\n";
    } else {
        echo "<h1>Ingrese los valores para x y n</h1>\n";
        echo "<form method=\"post\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\">\n";
        echo "  <label for=\"x\">x:</label>\n";
        echo "  <input type=\"number\" name=\"x\" id=\"x\" step=\"any\" required>\n";
        echo "  <br>\n";
        echo "  <label for=\"n\">n:</label>\n";
        echo "  <input type=\"number\" name=\"n\" id=\"n\" required>\n";
        echo "  <br>\n";
        echo "  <input type=\"submit\" value=\"Calcular\">\n";
        echo "</form>\n";
    }
    ?>
</body>

</html>