<?php
if(isset($_POST['btnCalcular'])){
    $notas = array(
        (float)$_POST['txtNota1'],
        (float)$_POST['txtNota2'],
        (float)$_POST['txtNota3'],
        (float)$_POST['txtNota4']
    );

    rsort($notas);

    $promedio = array_sum(array_slice($notas, 0, 3)) / 3;

    if ($promedio >= 11) {
        $mensaje = "Aprobado";
    } else {
        $mensaje = "Desaprobado";
    }
} else {
    $notas = array(0, 0, 0, 0);
    $promedio = 0;
    $mensaje = "";
}
?>

<html>
<head>
    <title>Ejercicio 16</title>
    <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
    <form method="POST">
        <label for="txtNota1">Ingrese la nota 1:</label>
        <input type="number" name="txtNota1" id="txtNota1" value="<?php echo $notas[0] ?>" required>
        <br>
        <label for="txtNota2">Ingrese la nota 2:</label>
        <input type="number" name="txtNota2" id="txtNota2" value="<?php echo $notas[1] ?>" required>
        <br>
        <label for="txtNota3">Ingrese la nota 3:</label>
        <input type="number" name="txtNota3" id="txtNota3" value="<?php echo $notas[2] ?>" required>
        <br>
        <label for="txtNota4">Ingrese la nota 4:</label>
        <input type="number" name="txtNota4" id="txtNota4" value="<?php echo $notas[3] ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <?php
    if(isset($_POST['btnCalcular'])){
        echo "<div class='contenedor'>";
        echo "<p>Promedio de las tres mejores notas: $promedio</p>";
        echo "<p>Resultado: $mensaje</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
