<!DOCTYPE html>
<html>

<head>
    <title>Promedio de matriz 3x2</title>
    <link rel="stylesheet" type="text/css" href="estilos57.css">
</head>

<body>
    <form method="post">
        <table>
            <?php for ($i = 0; $i < 3; $i++) : ?>
                <tr>
                    <?php for ($j = 0; $j < 2; $j++) : ?>
                        <td>
                            <input type="number" name="num[<?php echo $i; ?>][<?php echo $j; ?>]">
                        </td>
                    <?php endfor; ?>
                </tr>
            <?php endfor; ?>
        </table>
        <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $matrix = $_POST["num"];
        $sum = 0;
        foreach ($matrix as $row) {
            foreach ($row as $num) {
                $sum += $num;
            }
        }
        $avg = $sum / 6;
        echo "<div class='result-container'>El promedio aritmético de la matriz es: $avg</div>";
    }
    ?>
</body>

</html>