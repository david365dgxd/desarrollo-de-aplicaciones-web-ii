<!DOCTYPE html>
<html>
<link rel="stylesheet" href="estilos53.css">

<head>
    <title>Multiplos de n</title>
</head>

<body>
    <form method="post">
        <label for="n">ingresa n:</label>
        <input type="number" name="n" id="n"><br>

        <?php for ($i = 1; $i <= 6; $i++) : ?>
            <label for="num<?php echo $i; ?>">Number <?php echo $i; ?>:</label>
            <input type="number" name="num<?php echo $i; ?>" id="num<?php echo $i; ?>"><br>
        <?php endfor; ?>

        <input type="submit" value="Calculate">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $n = $_POST["n"];
        $vector = array();
        for ($i = 1; $i <= 6; $i++) {
            array_push($vector, $_POST["num$i"]);
        }
        $count = 0;
        foreach ($vector as $num) {
            if ($num % $n == 0) {
                $count++;
            }
        }
        echo "hay $count numeros que son multiplos de $n.";
    }
    ?>
</body>

</html>