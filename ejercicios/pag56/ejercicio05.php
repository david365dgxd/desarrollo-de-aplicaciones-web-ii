<?php
if (isset($_POST['btnCalcular'])) {
    //Obtener los valores ingresados por el usuario
    $a = (int)$_POST['txtA'];
    $b = (int)$_POST['txtB'];
    $c = (int)$_POST['txtC'];
    $d = (int)$_POST['txtD'];

    //Realizar las operaciones
    $suma = $a + $b + $c + $d;
    $porcentaje_a = round(($a / $suma) * 100, 2);
    $porcentaje_b = round(($b / $suma) * 100, 2);
    $porcentaje_c = round(($c / $suma) * 100, 2);
    $porcentaje_d = round(($d / $suma) * 100, 2);
} else {
    //Si el usuario no ha enviado el formulario, se asignan valores iniciales
    $a = 0;
    $b = 0;
    $c = 0;
    $d = 0;
    $suma = 0;
    $porcentaje_a = 0;
    $porcentaje_b = 0;
    $porcentaje_c = 0;
    $porcentaje_d = 0;
}
?>

<html>

<link rel="stylesheet" type="text/css" href="estilos05.css">

<head>
    <title>Ejercicio 05</title>

</head>

<body>
    <form method="POST">
        <label for="txtA">Ingrese el primer número:</label>
        <input type="number" name="txtA" id="txtA" value="<?php echo $a ?>" required>
        <br>
        <label for="txtB">Ingrese el segundo número:</label>
        <input type="number" name="txtB" id="txtB" value="<?php echo $b ?>" required>
        <br>
        <label for="txtC">Ingrese el tercer número:</label>
        <input type="number" name="txtC" id="txtC" value="<?php echo $c ?>" required>
        <br>
        <label for="txtD">Ingrese el cuarto número:</label>
        <input type="number" name="txtD" id="txtD" value="<?php echo $d ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <?php
    //Mostrar los resultados si se han realizado las operaciones
    if (isset($_POST['btnCalcular'])) {
        echo "<div class='contenedor'>";
        echo "<p>Los porcentajes son:</p>";
        echo "<ul>";
        echo "<li>$porcentaje_a%</li>";
        echo "<li>$porcentaje_b%</li>";
        echo "<li>$porcentaje_c%</li>";
        echo "<li>$porcentaje_d%</li>";
        echo "</ul>";
        echo "</div>";
    }
    ?>
</body>

</html>