<?php
if (isset($_POST['btnCalcular'])) {
    // Obtener los valores ingresados por el usuario
    $a = (int)$_POST['txtA'];
    $b = (int)$_POST['txtB'];

    // Inicializar contador de números enteros
    $count = 0;

    // Calcular cantidad de números enteros incluidos
    for ($i = $a; $i <= $b; $i++) {
        if (is_int($i)) {
            $count++;
        }
    }
} else {
    // Si el usuario no ha enviado el formulario, se asignan valores iniciales
    $a = 0;
    $b = 0;
    $count = 0;
}
?>

<html>

<link rel="stylesheet" type="text/css" href="estilos02.css">

<head>
    <title>Ejercicio 02</title>
</head>

<body>
    <form method="POST">
        <label for="txtA">Ingrese el primer numero:</label>
        <input type="number" name="txtA" id="txtA" value="<?php echo $a ?>" required>
        <br>
        <label for="txtB">Ingrese el segundo numero:</label>
        <input type="number" name="txtB" id="txtB" value="<?php echo $b ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <?php
    // Mostrar el resultado si se han realizado las operaciones
    if (isset($_POST['btnCalcular'])) {
        echo "La cantidad de números enteros incluidos entre $a y $b es: $count";
    }
    ?>
</body>

</html>