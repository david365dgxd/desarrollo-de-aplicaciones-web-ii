<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 19</title>
	<link rel="stylesheet" type="text/css" href="estilos19.css">
</head>
<body>
	<form method="POST">
		<label for="txtLado1">Ingrese la longitud del primer lado:</label>
		<input type="number" name="txtLado1" id="txtLado1" required>
		<br>
		<label for="txtLado2">Ingrese la longitud del segundo lado:</label>
		<input type="number" name="txtLado2" id="txtLado2" required>
		<br>
		<label for="txtLado3">Ingrese la longitud del tercer lado:</label>
		<input type="number" name="txtLado3" id="txtLado3" required>
		<br>
		<input type="submit" name="btnVerificar" value="Verificar">
	</form>
	<br>
	<?php
	if(isset($_POST['btnVerificar'])){
		$lado1 = (int)$_POST['txtLado1'];
		$lado2 = (int)$_POST['txtLado2'];
		$lado3 = (int)$_POST['txtLado3'];

		if (($lado1 < ($lado2 + $lado3)) && ($lado2 < ($lado1 + $lado3)) && ($lado3 < ($lado1 + $lado2))) {
			echo "<div class='contenedor'>";
			echo "<p>Las longitudes ingresadas forman un triángulo</p>";
			echo "</div>";
		} else {
			echo "<div class='contenedor'>";
			echo "<p>Las longitudes ingresadas no forman un triángulo</p>";
			echo "</div>";
		}
	}
	?>
</body>
</html>
