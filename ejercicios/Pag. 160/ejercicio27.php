<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Estado civil</title>
	<link rel="stylesheet" type="text/css" href="estilos27.css">
</head>
<body>
	<div class="container">
		<h1>Estado civil</h1>
		<form method="post" action="ejercicio27.php">
			<label for="codigo">Código:</label>
			<input type="number" id="codigo" name="codigo" min="0" max="3" required>
			<button type="submit">Obtener estado civil</button>
		</form>
		<?php
			if (isset($_POST['codigo'])) {
				$codigo = $_POST['codigo'];
				switch ($codigo) {
					case 0:
						$estado_civil = "Soltero";
						break;
					case 1:
						$estado_civil = "Casado";
						break;
					case 2:
						$estado_civil = "Divorciado";
						break;
					case 3:
						$estado_civil = "Viudo";
						break;
					default:
						$estado_civil = "Código inválido";
						break;
				}
		?>
				<p>El estado civil correspondiente al código <?php echo $codigo ?> es <?php echo $estado_civil ?>.</p>
		<?php
			}
		?>
	</div>
</body>
</html>
