<!DOCTYPE html>
<html>
<head>
	<title>Verificar dígito en número</title>
	<link rel="stylesheet" type="text/css" href="estilos35.css">
</head>
<body>
	<div class="container">
		<h1>Verificar dígito en número</h1>
		<form method="POST">
			<label for="numero">Ingrese un número:</label>
			<input type="number" name="numero" required>
			<label for="digito">Ingrese un dígito:</label>
			<input type="number" name="digito" required>
			<button type="submit">Verificar</button>
		</form>
		<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$numero = $_POST["numero"];
			$digito = $_POST["digito"];
			$contador = 0;
			while ($numero > 0) {
				if ($numero % 10 == $digito) {
					$contador++;
				}
				$numero = (int)($numero / 10);
			}
			echo "<p>El dígito $digito aparece $contador veces en el número ingresado.</p>";
		}
		?>
	</div>
</body>
</html>
