<?php
if(isset($_POST['btnEnviar'])){
    $numDia = (int)$_POST['txtNumDia'];

    switch($numDia){
        case 1:
            $dia = "Domingo";
            break;
        case 2:
            $dia = "Lunes";
            break;
        case 3:
            $dia = "Martes";
            break;
        case 4:
            $dia = "Miércoles";
            break;
        case 5:
            $dia = "Jueves";
            break;
        case 6:
            $dia = "Viernes";
            break;
        case 7:
            $dia = "Sábado";
            break;
        default:
            $dia = "Número de día inválido";
            break;
    }
} else {
    $numDia = 0;
    $dia = "";
}
?>

<html>
<head>
    <title>Ejercicio 22</title>
    <link rel="stylesheet" type="text/css" href="estilos22.css">
</head>
<body>
    <form method="POST">
        <label for="txtNumDia">Ingrese el número del día (1 al 7):</label>
        <input type="number" name="txtNumDia" id="txtNumDia" value="<?php echo $numDia ?>" required>
        <br>
        <input type="submit" name="btnEnviar" value="Enviar">
    </form>
    <br>
    <?php
    if(isset($_POST['btnEnviar'])){
        echo "<div class='contenedor'>";
        echo "<p>El día correspondiente es: $dia</p>";
        echo "</div>";
    }
    ?>
</body>
</html>
