<!DOCTYPE html>
<html>

<head>
    <title>Números repetidos</title>
    <link rel="stylesheet" href="estilos55.css">
</head>

<body>
    <div class="container">
        <h1>Números repetidos</h1>
        <form method="post">
            <label for="num1">Número 1:</label>
            <input type="number" name="num1" id="num1"><br>

            <label for="num2">Número 2:</label>
            <input type="number" name="num2" id="num2"><br>

            <label for="num3">Número 3:</label>
            <input type="number" name="num3" id="num3"><br>

            <label for="num4">Número 4:</label>
            <input type="number" name="num4" id="num4"><br>

            <label for="num5">Número 5:</label>
            <input type="number" name="num5" id="num5"><br>

            <label for="num6">Número 6:</label>
            <input type="number" name="num6" id="num6"><br>

            <input type="submit" value="Calcular">
        </form>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numeros = array();
            $repetidos = array();
            for ($i = 1; $i <= 6; $i++) {
                $num = $_POST["num$i"];
                if (in_array($num, $numeros) && !in_array($num, $repetidos)) {
                    array_push($repetidos, $num);
                }
                array_push($numeros, $num);
            }
            $count = count($repetidos);
            if ($count == 0) {
                echo "<div class='resultado'>No hay números repetidos.</div>";
            } else if ($count == 1) {
                echo "<div class='resultado'>Hay 1 número repetido.</div>";
            } else {
                echo "<div class='resultado'>Hay $count números repetidos.</div>";
            }
        }
        ?>
    </div>
</body>

</html>