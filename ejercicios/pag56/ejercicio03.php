<?php
if (isset($_POST['btnCalcular'])) {
    //Obtener los valores ingresados por el usuario
    $milimetros = (int)$_POST['txtMilimetros'];

    //Realizar las operaciones
    $metros = (int)($milimetros / 1000);
    $decimetros = (int)(($milimetros % 1000) / 100);
    $centimetros = (int)(($milimetros % 100) / 10);
    $milimetros_restantes = $milimetros % 10;
} else {
    //Si el usuario no ha enviado el formulario, se asigna valor inicial
    $milimetros = 0;
    $metros = 0;
    $decimetros = 0;
    $centimetros = 0;
    $milimetros_restantes = 0;
}
?>

<html>
<link rel="stylesheet" type="text/css" href="estilos03.css">

<head>
    <title>Ejercicio 03</title>

</head>

<body>
    <form method="POST">
        <label for="txtMilimetros">Ingrese la cantidad en milímetros:</label>
        <input type="number" name="txtMilimetros" id="txtMilimetros" value="<?php echo $milimetros ?>" required>
        <br>
        <input type="submit" name="btnCalcular" value="Calcular">
    </form>
    <br>
    <?php
    //Mostrar los resultados si se han realizado las operaciones
    if (isset($_POST['btnCalcular'])) {
        echo "<div class='contenedor'>";
        echo "<p>$milimetros milímetros son:</p>";
        echo "<ul>";
        echo "<li>$metros metro(s)</li>";
        echo "<li>$decimetros decímetro(s)</li>";
        echo "<li>$centimetros centímetro(s)</li>";
        echo "<li>$milimetros_restantes milímetro(s)</li>";
        echo "</ul>";
        echo "</div>";
    }
    ?>
</body>

</html>